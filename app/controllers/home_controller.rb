#HomeController est la classe du controleur Home
#Elle herite de ApplicationController

#APplication controller n'est pas un controleur mais une
#classe mere qui herite de ActionController::Base

#Cela permet de factoriser du code
#index est la methode de notre action

class HomeController < ApplicationController
	def index
	end
end